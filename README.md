# Drupal 8 install profile by Digity #

This is a custom install profile for websites created by Digity which requires and enables following
Drupal modules:

  - address
  - admin_toolbar
  - admin_toolbar_tools
  - adminimal_admin_toolbar
  - advagg
  - allowed_formats
  - block
  - block_content
  - ckeditor
  - config
  - contact
  - contact_storage
  - contextual
  - critical_css
  - ctools
  - datetime
  - dblog
  - devel
  - dynamic_page_cache
  - editor
  - email_registration
  - entity_reference_revisions
  - field_group
  - field_ui
  - file
  - geolocation
  - help
  - http2_server_push
  - image
  - image_widget_crop
  - imce
  - linkit
  - menu_admin_per_menu
  - menu_link_content
  - menu_ui
  - messageclose
  - metatag
  - node
  - options
  - override_node_options
  - page_cache
  - path
  - pathauto
  - paragraphs
  - paragraphs_edit
  - redirect
  - simple_sitemap
  - system_tags
  - tasty_backend
  - taxonomy
  - telephone
  - token
  - views
  - views_ui
  - quickedit
  - toolbar
  - twig_tweak
  
  The module will enable following themes:
  - basetheme (a custom theme by Digity) for the frontend
  - adminimal_theme for the backend
  
  Following settings are applied when using this install profile:
  - Authenticated users get permission to search content
  - All users get permission to use the contact page
  - Default timezone is set to Belgium / Brussels
  - Contact form mailto address automatically the emailaddress used on installation