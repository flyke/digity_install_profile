<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function digity_install_profile_form_install_select_profile_form_alter(&$form, FormStateInterface $form_state) {

}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function digity_install_profile_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
    $form['regional_settings']['site_default_country']['#default_value'] = 'BE';
    $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Brussels';
    // Apparently, the timezone is being set in core via Javascript.
    // That's why whe remove the following class, preventing Javascript to work properly
    $form['regional_settings']['date_default_timezone']['#attributes'] = ['class' => ['nomore-timezone-detect']];

    $form['#submit'][] = 'digity_install_profile_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function digity_install_profile_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}
